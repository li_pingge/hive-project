--一.初始化 创建模拟数据，copy到hdfs
--将 E:\JOB_shan\个人文件\副业大技\代码\20210521-hive\hive_数据源\最新  修改为本地地址
$ hadoop fs -mkdir /user
$ hadoop fs -mkdir /user/homework
$ hadoop fs -mkdir /user/homework/date
$ hadoop fs -mkdir /user/homework/order
$ hadoop fs -put E:\大三下学期hive项目date.csv /user/homework/date

$ hadoop fs -put E:\大三下学期hive项目order.csv /user/homework/order

--删除（可能需要）
$ hadoop fs -rm -r  /user/homework/date/date.csv
$ hadoop fs -rm -r  /user/homework/ordernew/ordernew.csv



--二.hive 创建库和表
--1.Hive 创建表代码，hdfs到hive中
---在外部表（原始日志表）的基础上做大量的统计分析，用到的中间表、结果表使用内部表存储，数据通过SELECT+INSERT进入内部表。
#创建库
 create database eco;
#使用库
 use eco;

#创建外部表
---order
create external  table order_ods
(order_date string comment '订单日期'
            ,year string comment '年份'
            ,quantity int comment '订单数量'
            ,product_id string comment '产品id'
            ,customer_id string comment '客户id'
            ,transaction_type string comment '交易类型'
            ,sales_area_id string comment '销售区域id'
            ,sales_region string comment '销售大区'
            ,country string comment '国家'
            ,region string comment '区域'
            ,product_category string comment '产品类别'
            ,product_model_name string comment '产品型号名称'
            ,product_name string comment '产品名称'
            ,product_cost int comment '产品成本'
            ,profit float comment '利润'
            ,Unit_Price float comment '单价'
            ,sales_amount float comment '销售金额'
)
row format delimited fields terminated by ',' 
stored as textfile location '/user/homework/order' ;
ALTER TABLE order_ods  SET SERDEPROPERTIES ('serialization.encoding'='gbk');

---date:
create external  table  date_ods
(ddate string comment '日期'
            ,year string comment '年度'
            ,quarter int comment '季度'
            ,month string comment '月份'
            ,day string comment '日'
            ,year_quarter string comment '年度季度'
            ,year_month string comment '年度月份'
            ,week_day string comment '星期几'
        )
row format delimited fields terminated by ',' 
stored as textfile location '/user/homework/date' ;
ALTER TABLE date_ods  SET SERDEPROPERTIES ('serialization.encoding'='gbk');

select   t.product_name,count(*)
from order_ods t
group by t.product_name
limit 10;

2.Hive 分析代码到hive 中间层  ： 创建内部表 分析插入到内部表
--(1) 计算出购买产品棒球手套的客户中，有多少客户也同时购买了产品硬式棒球
create table if not exists hard_baseball_uv_app(
    uv int comment '硬式棒球销售金额' 
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

with tmp1 as (SELECT t.customer_id
            FROM (select t.customer_id
                    from order_ods t
                    where t.product_name ='硬式棒球'
                    )t
            left join (select tc.customer_id
                        from  order_ods tc
                        where tc.product_name ='棒球手套'
                        ) tc
            on t.customer_id = tc.customer_id
            where tc.customer_id is not null
            group by t.customer_id
  )
insert overwrite table hard_baseball_uv_app
select count(*) uv,unix_timestamp() 
from tmp1;

 ---(2).购买球棒与球棒袋的金额有多大
create table if not exists  batbag_Amount_app(
     sales_amount float comment '球棒与球棒袋销售金额' 
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

insert overwrite  table batbag_Amount_app
 select cast(sum(nvl(t.sales_amount,0))  as decimal(20,4)),unix_timestamp() 
  from order_ods t
  where t.product_name ='球棒与球棒袋';

---(3).来自大中华区的顾客有多少 sales_region
create table if not exists  greaterchina_uv_app(
     uv int comment '大中华区的顾客' 
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

with tmp1 as (
        select t.customer_id
                from order_ods t
                where t.region ='大中华区'
                group by t.customer_id
         )
insert overwrite  table greaterchina_uv_app
select count(*) uv,unix_timestamp() 
from tmp1;

---(4).查询同一个产品不同客户的订单金额
--- 产品  订单金额  
create table if not exists  product_amount_app(
     product_name string comment '产品名称'
    ,sum_sales_amount float comment '订单金额'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;
insert overwrite  table product_amount_app
select t.product_name,cast(sum(nvl(t.sales_amount,0))  as decimal(20,4)) sum_sales_amount,unix_timestamp() 
from order_ods t
group by t.product_name;


---(5).查询每个客户购买相同物品的次数
----  客户 产品 购买次数
create table if not exists  cust_product_pv_app(
    customer_id string comment '客户id'
    ,product_name string comment '产品名称'
    ,pv int comment '购买次数'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;
insert overwrite  table cust_product_pv_app
select t.product_name,t.customer_id,count(*) pv,unix_timestamp()
from order_ods t
group by t.product_name,t.customer_id;


---(6).查询每一季度客户购买棒球手套总金额（呈现一个折线图或柱状图）
create table if not exists  quarter_ballgloves_amount_app(
    year_quarter string comment '年度季度'
    ,sum_sales_amount float comment '棒球手套总金额'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

insert overwrite  table quarter_ballgloves_amount_app
select tc.year_quarter,cast(sum(nvl(t.sales_amount,0)) as decimal(20,4)) sum_sales_amount,unix_timestamp()
from (select *
        from order_ods t
      where t.product_name ='棒球手套'
      ) t
inner join date_ods tc
    on t.order_date = tc.ddate
group by tc.year_quarter;

 ---(7).查询销售额最高的前三个产品
create table if not exists  amount_producttop3_app(
    product_name string comment '产品名称'
    ,sum_sales_amount float comment '销售额最高'
    ,rn int comment '排名'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

insert overwrite  table amount_producttop3_app
select t.product_name ,t.sum_sales_amount,t.rn,unix_timestamp()
from (select t.product_name ,cast(sum(nvl(t.sales_amount,0))  as decimal(20,4)) sum_sales_amount
            ,row_number() over(order by cast(sum(nvl(t.sales_amount,0))  as decimal(20,4))  desc ) rn
        from order_ods t
      group by  t.product_name
      ) t
where t.rn <4;

---(8).创建一个订单数据表（订单编号、物品名称、物品单价、物品数量）
--- order.csv 的 订单日期||客户id 变成订单编号，然后按订单编号 物品名称 物品单价来求  物品数量
create table if not exists  orders_dwm(
    orders_id string comment '订单编号'
    ,product_name string comment '产品名称'
    ,Unit_Price float comment '单价'
    ,pv int comment '物品数量'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

insert overwrite  table orders_dwm
select  concat(t.order_date,'_',t.customer_id) orders_id, t.product_name,t.Unit_Price
        ,count(*) pv,unix_timestamp()
from order_ods t
group by concat(t.order_date,'_' ,t.customer_id),t.product_name,t.Unit_Price;


 ---(9).按天查询订单总金额  
---小数点：DECIMAL(9,8)代表最多9位数字，后8位是小数。此时也就是说，小数点前最多有1位数字，如果超过一位则会变成null。
create table if not exists  order_amount_d_app(
    order_date string comment '订单日期'
    ,sum_sales_amount float comment '订单金额'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

insert overwrite table order_amount_d_app
select t.order_date ,cast(sum(nvl(t.sales_amount,0))  as decimal(20,4)) sum_sales_amount,unix_timestamp()
from order_ods t
group by t.order_date;


 ---(10).查询每一季度销售额最高的产品
create table if not exists  quarter_protop1_app(
    year_quarter string comment '年度季度'
    ,product_name string comment '产品名称'
    ,sum_sales_amount float comment '订单金额'
    ,add_time string comment '入库时间'
)
row format delimited fields terminated by ','
stored as textfile;

with tmp1 as (select tc.year_quarter,t.product_name,cast(sum(nvl(t.sales_amount,0))  as decimal(20,4)) sum_sales_amount
            ,row_number() over(partition by tc.year_quarter order by cast(sum(nvl(t.sales_amount,0))  as decimal(20,4))  desc ) rn
from  order_ods t
inner join date_ods tc
    on t.order_date = tc.ddate
    group by tc.year_quarter,t.product_name
)
insert overwrite table quarter_protop1_app
select t.year_quarter,t.product_name,t.sum_sales_amount,unix_timestamp()
from tmp1 t
where t.rn =1;


----简单输出查看结果：
select * from hard_baseball_uv_app limit 10;
select * from batbag_Amount_app limit 10;
select * from greaterchina_uv_app limit 10;
select * from product_amount_app limit 10;
select * from cust_product_pv_app limit 10;
select * from quarter_ballgloves_amount_app limit 10;
select * from amount_producttop3_app limit 10;
select * from orders_dwm limit 10;
select * from order_amount_d_app limit 10;
select * from quarter_protop1_app limit 10;


三.hive元数据查看
---查看：（mysql下）
use hive;
SELECT
    a. NAME as SCHEMA_NAME,-- 数据库名称
    t.TBL_NAME as TABLE_NAME,-- 表名
    b.PARAM_VALUE as TABLE_COMMENT,-- 表注释
    e.INTEGER_IDX as COLUMN_ID,-- 字段序号
    e.COLUMN_NAME ,-- 字段名
    e.TYPE_NAME as COLUMN_DATA_TYPE,-- 字段类型
    f.PART_KEYS as PART_COLUMN,-- 分区字段
    e.COMMENT-- 字段注释
FROM
    TBLS t
    JOIN DBS a ON t.DB_ID = a.DB_ID
    LEFT JOIN (select TBL_ID,PARAM_VALUE FROM TABLE_PARAMS where PARAM_KEY='comment') b ON t.TBL_ID = b.TBL_ID
    LEFT JOIN SDS c ON t.SD_ID = c.SD_ID
    LEFT JOIN CDS d ON c.CD_ID = d.CD_ID
    LEFT JOIN COLUMNS_V2 e ON d.CD_ID = e.CD_ID
    LEFT JOIN (SELECT TBL_ID,GROUP_CONCAT(PKEY_NAME) AS PART_KEYS FROM PARTITION_KEYS  GROUP BY TBL_ID) f on t.TBL_ID = f.TBL_ID
		#此处选择你需要的库
    where a. NAME = 'eco' 
    order by 1,2,4;

四.问题说明
1. hive中字段备注乱码
--操作方法：在mysql中，修改如下，再次创建，则没有问题
--a.      修改字段注释字符集
alter table COLUMNS_V2 modify column COMMENT varchar(256) character set utf8;
--b.      修改表注释字符集
alter table TABLE_PARAMS modify column PARAM_VALUE varchar(4000) character set utf8;
--c.      修改分区表参数，以支持分区键能够用中文表示
alter table PARTITION_PARAMS modify column PARAM_VALUE varchar(4000) character set utf8;
alter table PARTITION_KEYS modify column PKEY_COMMENT varchar(4000) character set utf8;
--d.      修改索引注解
alter table INDEX_PARAMS modify column PARAM_VALUE varchar(4000) character set utf8;




