/*创建客户表*/
create table customer(
customer_id string
)ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|';

/*创建日期表*/
create table date_table(
day_date string,
year_date string,
quarter string,
month_date int,
ri_date int,
year_quarter string,
year_month string,
week_date int
)ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|';

/*创建订单表*/
create table order_table(
order_date string,
year_date int,
order_num int,
product_id string,
customer_id string,
trade_type int,
market_area_id int,
market_area string,
market_country string,
market_big_area string,
product_class string,
product_model string,
product_name string,
cost int,
profit int,
price int,
sales_amount int
)ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|';

/*创建产品信息表*/
create table product_infor(
product_class string,
product_id string,
product_model string,
product_name string
)ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|';

drop table customer;
drop table order_table;

select * from customer;
select * from order_table;
select * from date_table;
select * from product_infor;

load data local inpath '/home/niit/hivedata/customer.txt' into table customer;
load data local inpath '/home/niit/hivedata/date.txt' into table date_table;
load data local inpath '/home/niit/hivedata/order.txt' into table order_table;
load data local inpath '/home/niit/hivedata/product.txt' into table product_infor;

--(1) 计算出购买产品棒球手套的客户中，有多少客户也同时购买了产品硬式棒球
with tmp1 as (SELECT t.customer_id
            FROM (select t.customer_id
                    from order_table t
                    where t.product_name ='硬式棒球'
                    )t
            left join (select tc.customer_id
                        from  order_table tc
                        where tc.product_name ='棒球手套'
                        ) tc
            on t.customer_id = tc.customer_id
            where tc.customer_id is not null
            group by t.customer_id
  )
select count(*) as uv
from tmp1;

 ---(2).购买球棒与球棒袋的金额有多大
 select sum(sales_amount * price) as total_money
 from order_table
 where product_name='球棒与球棒袋';

---(3).查询每一季度客户购买棒球手套总金额（呈现一个折线图或柱状图）
select year_quarter,sum(sales_amount * price) as sum_sales_amount
from (select * from order_table
      where product_name ='棒球手套'
      ) t
inner join date_table tc on t.order_date = tc.day_date
group by tc.year_quarter;

 ---(4).查询销售额最高的前三个产品
select product_name ,sum_sales_amount
from (select product_name ,sum(sales_amount * price)  as sum_sales_amount
      from order_table 
      group by product_name
      order by sum_sales_amount DESC 
      limit 3
      ) t;
     

---(5).查询客户13021BA购买相同物品的次数
select product_name,customer_id,count(*) as total
from order_table
where customer_id ='13021BA'
group by product_name,customer_id;


---(6).来自大中华区的顾客有多少 sales_region
select count(*) as total_num
from(select customer_id,market_big_area
     from order_table
     group by customer_id,market_big_area
     having market_big_area ='大中华区'
     ) temp;
    
---(7).查询每个产品的订单总金额
select product_name,sum(sales_amount * price) as sum_sales_amount
from order_table
group by product_name;

--- (8)order.csv 的 订单日期||客户id 变成订单编号，然后按订单编号 物品名称来求  物品数量     
select  concat(t.order_date,'_',t.customer_id) order_id, t.product_name
        ,count(*) as total
from order_table t
group by concat(t.order_date,'_' ,t.customer_id),t.product_name;     

---(9).按天查询订单总金额(2013.10月份的前10天)(折线图可视化)
select order_date ,sum(sales_amount * price) sum_sales_amount
from order_table
group by order_date
order by order_date ASC 
limit 10;

 ---(10).查询每一季度销售量最高的产品（柱状图可视化）

select year_quarter,product_name, sum_sales_amount, rank_num
from(select year_quarter,product_name, sum_sales_amount,rank() over (partition by year_quarter order by sum_sales_amount desc ) as rank_num
     from(select year_quarter,product_name, sum(sales_amount) as sum_sales_amount 
          from order_table 
          inner join date_table  on order_date = day_date
          group by year_quarter,product_name
     )as temp1
)as temp2
where rank_num =1;


